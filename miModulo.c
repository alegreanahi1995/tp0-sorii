#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>        /* for put_user */
#include <linux/ctype.h> // isalpha, isupper


int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
int ord(char c);
void cifrar(const char *mensaje, char *destino, int rotaciones);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "chardev"   /* Dev name as it appears in /proc/devices   */
#define BUF_LEN 80              /* Max length of the message from the device */


#define PROCFS_MAX_SIZE         1024
#define PROCFS_NAME             "buffer1k"


#define LONGITUD_ALFABETO 26
#define INICIO_ALFABETO_MAYUSCULAS 65
#define INICIO_ALFABETO_MINUSCULAS 97
/* 
 * Global variables are declared as static, so are global within the file. 
 */

int rotaciones=4;
static int Major=100;               /* Major number assigned to our device driver */
static int Device_Open = 0;     /* Is device open?  
                                 * Used to prevent multiple access to device */
static char msg[BUF_LEN];       /* The msg the device will give when asked */
static char *msg_Ptr;
static unsigned long procfs_buffer_size = 0;
static unsigned long mensajecif_buffer_size = 0;
static char procfs_buffer[PROCFS_MAX_SIZE];
static char mensajecif[PROCFS_MAX_SIZE];
const char *alfabetoMinusculas = "abcdefghijklmnopqrstuvwxyz",
*alfabetoMayusculas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

static struct file_operations fops = {
        .read = device_read,
        .write = device_write,
        .open = device_open,
        .release = device_release
};
/*
 * This function is called when the module is loaded
 */
int init_module(void)
{


        Major = register_chrdev(0, DEVICE_NAME, &fops);
        if (Major < 0) {
          printk(KERN_ALERT "Registering char device failed with %d\n", Major);
          return Major;
        }

 	printk(KERN_INFO "UNGS : Driver registrado\n");
        printk(KERN_INFO "I was assigned major number %d. To talk to\n", Major);
        printk(KERN_INFO "the driver, create a dev file with\n");
        printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", DEVICE_NAME, Major);
        printk(KERN_INFO "Try various minor numbers. Try to cat and echo to\n");
        printk(KERN_INFO "the device file.\n");
        printk(KERN_INFO "Remove the device file and module when done.\n");
        return SUCCESS;
}



/*
 * This function is called when the module is unloaded
 */
void cleanup_module(void)
{

printk(KERN_ALERT "Major : %d\n", Major);
        
         /* Unregister the device 
         */

       unregister_chrdev(Major,DEVICE_NAME);
       printk(KERN_INFO "UNGS : Driver desregistrado\n");
}


static int device_open(struct inode *inode, struct file *file)
{
        static int counter = 0;
        if (Device_Open)
                return -EBUSY;
        Device_Open++;
 	 sprintf(msg, "I already told you %d times Hello world!\n", counter++);       
        msg_Ptr = msg;
        try_module_get(THIS_MODULE);
        return SUCCESS;
}
/* 
 * Called when a process closes the device file.
 */
static int device_release(struct inode *inode, struct file *file)
{


        Device_Open--;          /* We're now ready for our next caller */
        /* 
        Decrement the usage count, or else once you opened the file, you'll
         * never get get rid of the module. 
         */
        module_put(THIS_MODULE);
        return 0; 
}

static ssize_t device_read(struct file *filp,   /* see include/linux/fs.h   */
                           char *buffer,        /* buffer to fill with data */
                           size_t length,       /* length of the buffer     */
                           loff_t * offset)
{


static int finished = 0;
        /* 
         * We return 0 to indicate end of file, that we have
         * no more information. Otherwise, processes will
         * continue to read from us in an endless loop. 
         */
        if ( finished ) {
                printk(KERN_INFO "procfs_read: END\n");
                finished = 0;
                return 0;
        }
        finished = 1;
        
	if(procfs_buffer_size==0)
	return 0;
   mensajecif_buffer_size=procfs_buffer_size;
	cifrar(procfs_buffer, mensajecif, rotaciones);

        if ( copy_to_user(buffer, mensajecif, procfs_buffer_size) ) {
                return -EFAULT;
        }
        printk(KERN_INFO "Leidos %lu bytes\n", procfs_buffer_size);
        return procfs_buffer_size; 
}

 


 //Called when a process writes to dev file: echo "hi" > /dev/hello 
 
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{


        
        if ( len > PROCFS_MAX_SIZE )    {
                procfs_buffer_size = PROCFS_MAX_SIZE;
        }
        else    {
                procfs_buffer_size = len;

        }

        if ( copy_from_user(procfs_buffer, buff, procfs_buffer_size) ) {
                return -EFAULT;
        }
        printk(KERN_INFO "Escrito %lu bytes\n", procfs_buffer_size);

        printk(KERN_INFO "EL mensaje escrito es: %s", procfs_buffer);
        return procfs_buffer_size;


}




void cifrar(const char *mensaje, char *destino, int rotaciones) {
 
  int i = 0;
  while (mensaje[i]) {
    char caracterActual = mensaje[i];
    int posicionOriginal = ord(caracterActual);
    if (!isalpha(caracterActual)) {
      destino[i] = caracterActual;
      i++;
      continue; 
    }
    if (isupper(caracterActual)) {
      destino[i] =
          alfabetoMayusculas[(posicionOriginal - INICIO_ALFABETO_MAYUSCULAS +
                              rotaciones) %
                             LONGITUD_ALFABETO];
    } else {

      destino[i] =
          alfabetoMinusculas[(posicionOriginal - INICIO_ALFABETO_MINUSCULAS +
                              rotaciones) %
                             LONGITUD_ALFABETO];
    }
    i++;
  }
}

int ord(char c) { return (int)c; }

MODULE_LICENSE("GPL");
MODULE_AUTHOR("UNGS");
MODULE_DESCRIPTION("Un primer driver");
