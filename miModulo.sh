#!/bin/bash
# -*- ENCODING: UTF-8 -*-


#echo "TP CHAR DEVICE- SORII"
#echo "MENÚ"
#sudo su
#make clean
#make

#cd /dev
#rm chardev
#rmmod miModulo
#cd $pwd
#insmod miModulo.ko
#cd /dev
#mknod chardev c 245 0
#echo "Ingresar Mensaje a Escribir"
#read mensaje
#echo $mensaje >> chardev
#cat chardev

path="$(pwd)/"

#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  TP CHAR DEVICE- SORII ";

 
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  CARGAR MODULO";
    echo -e "\t\t\t b.  ELIMINAR MODULO";
    echo -e "\t\t\t c.  ESCRIBIR";
    echo -e "\t\t\t d.  LEER";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------


imprimir_encabezado () {
    clear;
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}


a_funcion () {
        imprimir_encabezado "\tOpción a.  CARGAR MODULO";

	make
	insmod miModulo.ko
    echo "";
	echo "Ingresar el Major del dispositivo: "
	read major
	cd /dev
	mknod chardev c $major 0
	chmod 777 chardev
}


b_funcion () {
        imprimir_encabezado "\tOpción b.  ELIMINAR MODULO";
	cd /dev
	rm chardev
	cd $path
	rmmod miModulo
	make clean
}



c_funcion () {
        imprimir_encabezado "\tOpción c.  ESCRIBIR";
	cd /dev
	echo "Ingresar Mensaje a Escribir: "
	read mensaje
	echo $mensaje > chardev
	cd $path

}



d_funcion () {
       imprimir_encabezado "\tOpción d.  LEER ";
	cd /dev
	echo "El mensaje fue encriptado a: "$(cat chardev)""
	cd $path
}


esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}


#------------------------------------------------------
# TP CHARDEVICE- SORII
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done

exit
